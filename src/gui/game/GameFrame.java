package gui.game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GameFrame extends JFrame {
    private Timer timer;
    private GameComponent gameComponent;

    public GameFrame() {
        System.out.println("Start new game");

        gameComponent = new GameComponent();
        timer = new Timer(20, e -> gameComponent.update());

        add(gameComponent);
        timer.start();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                System.out.println("Exit game");
                timer.stop();
                dispose();
            }
        });

        setSize(600, 500);
        setMinimumSize(new Dimension(500, 400));
        setVisible(true);
    }
}
