package gui.game;

import javax.swing.*;

public class StartGame {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(
                () -> new GameFrame());
    }
}
